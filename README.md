# plugin.video.piped
[Piped](https://github.com/TeamPiped/Piped) Addon for Kodi.

This plugin is a **very** barebones Piped API client for Kodi using my [fork](https://gitdab.com/cere/python-piped-api-client) of the [Piped API Python Client](https://github.com/SKevo18/python-piped-api-client)

Download the latest version from [here](https://gitdab.com/cere/plugin.video.piped/archive/master.zip)

Piped API URL can be set in the addon's configuration

Don't expect support for this addon it's mainly for my personal use.

[Piped API instances](https://github.com/TeamPiped/Piped/wiki/Instances)